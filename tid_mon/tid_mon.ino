#include <EEPROM.h>
#include <avr/pgmspace.h>


#define d_show_seconds 0
#define d_show_minutes 1
#define d_show_hours 2
#define d_use_minutes 3 
#define d_sample_frequency 4
#define d_sample_frequency_mask 0b111111

#define d_bit_mask 0b1
#define d_byte_mask 0b11111111

#define d_data_header_timer_configuration 0b11
#define d_data_header_channel_configuration 0b10
#define d_data_header_timestamp 0b01
#define d_data_header 0b00
#define d_data_header_offset 14
#define d_data_parity_offset 13
#define d_data_channel_offset 10

#define d_eeprom_configuration_size 4
#define d_eeprom_data_start_address d_eeprom_configuration_size
#define d_eeprom_size 1024

#define d_ascii_offset 48

volatile byte the_seconds = 0;
volatile byte the_minutes = 0;
volatile byte the_hours = 0;

volatile unsigned int the_timer_configuration =0;
volatile unsigned int the_channel_configuration=0;
volatile byte the_minutes_alarm_buffer = 0;
volatile byte the_seconds_alarm_buffer = 0;
volatile bool the_reset_flag = 1;
volatile unsigned int the_eeprom_data_pointer = 0;
const int the_adc_pin_lut[8] = {A0,A1,A2,A3,A4,A5,A6,A7};

void setup() {

  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // prints title with ending line break
  Serial.println("serial port initialized!");
  
  read_configuration();
  Serial.println("configuration read!");

  the_eeprom_data_pointer=get_eeprom_data_pointer();
  Serial.println("data pointers updated!");

  cli();//stop interrupts
  Serial.println("stopping interrupts!");
  //set timer0 interrupt at 2kHz
    TCCR0A = 0;// set entire TCCR2A register to 0
    TCCR0B = 0;// same for TCCR2B
    TCNT0  = 0;//initialize counter value to 0
    // set compare match register for 2khz increments
    OCR0A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256)
    // turn on CTC mode
    TCCR0A |= (1 << WGM01);
    // Set CS01 and CS00 bits for 64 prescaler
    TCCR0B |= (1 << CS01) | (1 << CS00);   
    // enable timer compare interrupt
    TIMSK0 |= (1 << OCIE0A);
  Serial.println("timer0 configured!");
  //set timer1 interrupt at 1Hz
    TCCR1A = 0;// set entire TCCR1A register to 0
    TCCR1B = 0;// same for TCCR1B
    TCNT1  = 0;//initialize counter value to 0
    // set compare match register for 1hz increments
    OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (must be <65536)
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS12 and CS10 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);  
    // enable timer compare interrupt
    TIMSK1 |= (1 << OCIE1A);
  Serial.println("timer1 configured!");  
  //set timer2 interrupt at 8kHz
    TCCR2A = 0;// set entire TCCR2A register to 0
    TCCR2B = 0;// same for TCCR2B
    TCNT2  = 0;//initialize counter value to 0
    // set compare match register for 8khz increments
    OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
    // turn on CTC mode
    TCCR2A |= (1 << WGM21);
    // Set CS21 bit for 8 prescaler
    TCCR2B |= (1 << CS21);   
    // enable timer compare interrupt
    TIMSK2 |= (1 << OCIE2A);
  Serial.println("timer2 configured!");
  
  sei();//allow interrupts
  Serial.println("starting interrupts!");
}//end setup

ISR(TIMER0_COMPA_vect){//timer0 interrupt 2kHz toggles pin 8
//generates pulse wave of frequency 2kHz/2 = 1kHz (takes two cycles for full wave- toggle high then toggle low)
//Serial.println("timer0");
}

ISR(TIMER1_COMPA_vect){//timer1 interrupt 1Hz toggles pin 13 (LED)
//generates pulse wave of frequency 1Hz/2 = 0.5kHz (takes two cycles for full wave- toggle high then toggle low)
if (the_seconds<59) {
  the_seconds++;
}
else {
  the_seconds=0;
  if (the_minutes<59) {
    the_minutes++;    
    }
  else {
    the_hours++;
    the_minutes=0;
    }
  }

// check and write data
if ((the_timer_configuration>>d_use_minutes)&d_bit_mask) {
  if (the_minutes_alarm_buffer == 0){
    
    the_minutes_alarm_buffer=((the_timer_configuration>>d_sample_frequency)&d_sample_frequency_mask);
    if (the_eeprom_data_pointer<d_eeprom_size-4){
      for (byte c=0;c<8;c++){
        if ((the_channel_configuration>>c)&d_bit_mask){
          Serial.print(c);
          Serial.println("-channel sampled!");
          write_eeprom_int(the_eeprom_data_pointer, process_data(0,c,analogRead(the_adc_pin_lut[c])));
          the_eeprom_data_pointer+=2;
        }
      }
    }
    else {
      Serial.println("memory full! download data and format the eeprom!");
    }
    
  }
  else {
    if (the_seconds==0) {the_minutes_alarm_buffer -=1;}
  }
}
else {
  if (the_seconds_alarm_buffer == 0){
    //Serial.println("seconds sampled!");
    the_seconds_alarm_buffer= ((the_timer_configuration>>d_sample_frequency)&d_sample_frequency_mask);
    if (the_eeprom_data_pointer<d_eeprom_size-4){
      for (byte c=0;c<8;c++){
        if ((the_channel_configuration>>c)&d_bit_mask){
          Serial.print(c);
          Serial.println("-channel sampled!");
          write_eeprom_int(the_eeprom_data_pointer, process_data(0,c,analogRead(the_adc_pin_lut[c])));
          the_eeprom_data_pointer+=2;
        }
      }
    }
    else {
      Serial.println("memory full! download data and format the eeprom!");
    }
  }
  else {
    the_seconds_alarm_buffer -=1;
  }
}

//if ((the_timer_configuration >> d_show_hours)&&1){
//Serial.println("hours: " + String(the_hours));}
//if ((the_timer_configuration >> d_show_minutes)&&1){
//Serial.println("minutes: " + String(the_minutes));}
//if ((the_timer_configuration >> d_show_seconds)&&1){
//Serial.println("seconds: " + String(the_seconds));}
//




//Serial.println("timer1");

}

  
ISR(TIMER2_COMPA_vect){//timer1 interrupt 8kHz toggles pin 9
//generates pulse wave of frequency 8kHz/2 = 4kHz (takes two cycles for full wave- toggle high then toggle low)
//Serial.println("timer2");

}

void show_time(){
  
Serial.println("hours: " + String(the_hours));
Serial.println("minutes: " + String(the_minutes));
Serial.println("seconds: " + String(the_seconds));

  }


unsigned int read_eeprom_int(unsigned int address) {
  
  //byte the_memory_value_lsb_buffer;
  //byte the_memory_value_msb_buffer;
  unsigned int the_memory_value_buffer;
  //the_memory_value_lsb_buffer=(EEPROM.read(address+1));
  //the_memory_value_msb_buffer=(EEPROM.read(address));
  the_memory_value_buffer=((EEPROM.read(address))<<8) | (EEPROM.read(address+1));
  //the_memory_value_buffer=(the_memory_value_msb_buffer<<8) | the_memory_value_lsb_buffer;
  //return the_memory_value_msb_buffer;
  return the_memory_value_buffer;
  }


void write_eeprom_int(unsigned int address, unsigned int value) {
  
  byte the_memory_value_lsb_buffer, the_memory_value_lsb_verify_buffer;
  byte the_memory_value_msb_buffer, the_memory_value_msb_verify_buffer;

  the_memory_value_lsb_buffer=value & 0xff;
  the_memory_value_msb_buffer=(value>>8) & 0xff;
  
  the_memory_value_msb_verify_buffer=EEPROM.read(address);
  the_memory_value_lsb_verify_buffer=EEPROM.read(address+1);
  
  //Serial.println(value);
  //Serial.println(the_memory_value_lsb_buffer);
  //Serial.println(the_memory_value_msb_buffer);
  
  if (the_memory_value_msb_buffer==the_memory_value_msb_verify_buffer){
    //Serial.print("no_write: ");
    //Serial.println(address);
    }
  else{EEPROM.write(address,the_memory_value_msb_buffer);}

  if (the_memory_value_lsb_buffer==the_memory_value_lsb_verify_buffer){
    //Serial.print("no_write: ");
    //Serial.println(address+1);
    }
  else {EEPROM.write(address+1,the_memory_value_lsb_buffer);}
  

  }

void format_eeprom() {

  for (unsigned int the_memory_address_buffer=d_eeprom_data_start_address;the_memory_address_buffer<d_eeprom_size-d_eeprom_data_start_address;the_memory_address_buffer+=2){
    write_eeprom_int(the_memory_address_buffer,0xffff);
  }
  
  }


void show_eeprom(int start_address, int end_address){
  unsigned int the_memory_value_buffer;
  if (end_address>d_eeprom_size-d_eeprom_data_start_address){end_address=d_eeprom_size-d_eeprom_data_start_address;}
  
  for (unsigned int the_memory_address_buffer=start_address;the_memory_address_buffer<end_address;the_memory_address_buffer+=2){    
    the_memory_value_buffer=read_eeprom_int(the_memory_address_buffer);
    Serial.print(the_memory_address_buffer); 
    Serial.print('-');
    //Serial.print(the_memory_value_buffer<<8);
    Serial.print(the_memory_value_buffer);
    Serial.print('\n');
    //delay(1000);
    }
  
  }

void read_configuration(){
  unsigned int the_timer_configuration_buffer, the_channel_configuration_buffer;
  
  the_timer_configuration_buffer = read_eeprom_int(0);
  the_channel_configuration_buffer = read_eeprom_int(2);

  if (the_timer_configuration_buffer>>d_data_header_offset!=d_data_header_timer_configuration){Serial.println("eeprom timer config data corrupted!" );}
  if (the_channel_configuration_buffer>>d_data_header_offset!=d_data_header_channel_configuration){Serial.println("eeprom channel config data corrupted!" );}
  
  the_timer_configuration = the_timer_configuration_buffer & 0b11111111111111; //read_eeprom_int(0);
  the_channel_configuration = the_channel_configuration_buffer & 0b11111111111111; //read_eeprom_int(2);
  
  }

void write_configuration(){
  
  write_eeprom_int(0, the_timer_configuration | ((unsigned int)(d_data_header_timer_configuration)<<d_data_header_offset));
  write_eeprom_int(2, the_channel_configuration | ((unsigned int)(d_data_header_channel_configuration)<<d_data_header_offset));
  
  }

bool get_parity(unsigned int value){
  bool parity_buffer=0;
  parity_buffer = ((value<<0)&d_bit_mask)^((value<<1)&d_bit_mask)^((value<<2)&d_bit_mask)^((value<<3)&d_bit_mask)^((value<<4)&d_bit_mask)^((value<<5)&d_bit_mask)^((value<<6)&d_bit_mask)^((value<<7)&d_bit_mask)^((value<<8)&d_bit_mask)^((value<<9)&d_bit_mask)^((value<<10)&d_bit_mask)^((value<<11)&d_bit_mask)^((value<<12)&d_bit_mask)^((value<<13)&d_bit_mask)^((value<<14)&d_bit_mask)^((value<<15)&d_bit_mask);
  return parity_buffer;
  }

unsigned int get_eeprom_data_pointer(){
  unsigned int the_memory_value_buffer, the_memory_address_buffer;  
  byte the_header_buffer=0;
  bool the_parity_buffer=0;
  unsigned int the_timer_configuration_count, the_channel_configuration_count, the_data_count, the_timestamp_count =0;
  
  for (the_memory_address_buffer=d_eeprom_data_start_address;the_memory_address_buffer<d_eeprom_size-d_eeprom_data_start_address;the_memory_address_buffer+=2){
    //Serial.println(the_memory_address_buffer);
    the_memory_value_buffer=read_eeprom_int(the_memory_address_buffer);
    //Serial.println(the_memory_value_buffer);
    if (the_memory_value_buffer == 0xffff){
      Serial.print("bytes used: ");
      Serial.println(the_memory_address_buffer);
      return the_memory_address_buffer;
      }
    else {
      the_header_buffer=(the_memory_value_buffer>>d_data_header_offset) & 0b11;
      
      switch (the_header_buffer){
        case d_data_header_timer_configuration:
          the_timer_configuration_count++;
          //Serial.println(the_header_buffer);
          
        case d_data_header_channel_configuration:
          the_channel_configuration_count++;
        
        case d_data_header_timestamp:
          the_timestamp_count++;
        
        case d_data_header:
          the_parity_buffer = get_parity(the_memory_value_buffer&0b1111111111111);
          //Serial.println(the_data_count);
          if (the_parity_buffer == (the_memory_value_buffer>>d_data_parity_offset)&d_bit_mask){
            the_data_count++;
          }
          else {
            Serial.println(" - parity failed!");
            }
        }
      }
    }
    
  Serial.print(the_timer_configuration_count);
  Serial.println(" timer configurations");
  Serial.print(the_channel_configuration_count);
  Serial.println(" channel configurations");
  Serial.print(the_timestamp_count);
  Serial.println(" timestamp configurations");
  Serial.print(the_data_count);
  Serial.println(" data samples");
  Serial.println("memory full! download data and format eeprom!");
  return d_eeprom_size-d_eeprom_data_start_address;
  
  }

void interpret_eeprom(){
  unsigned int the_memory_value_buffer, the_memory_address_buffer;  
  byte the_header_buffer=0;
  bool the_parity_buffer=0;
  unsigned int the_timer_configuration_count, the_channel_configuration_count, the_data_count, the_timestamp_count =0;
  
  for (the_memory_address_buffer=d_eeprom_data_start_address;the_memory_address_buffer<d_eeprom_size-d_eeprom_data_start_address;the_memory_address_buffer+=2){
    //Serial.println(the_memory_address_buffer);
    the_memory_value_buffer=read_eeprom_int(the_memory_address_buffer);
    //Serial.println(the_memory_value_buffer);
    if (the_memory_value_buffer == 0xffff){
      Serial.print("bytes analized: ");
      Serial.println(the_memory_address_buffer);
      return;
      }
    else {
      the_header_buffer=(the_memory_value_buffer>>d_data_header_offset) & 0b11;
      
      switch (the_header_buffer){
        case d_data_header_timer_configuration:
          the_timer_configuration_count++;
          //Serial.println(the_header_buffer);
          
        case d_data_header_channel_configuration:
          the_channel_configuration_count++;
        
        case d_data_header_timestamp:
          the_timestamp_count++;
        
        case d_data_header:
          the_parity_buffer = get_parity(the_memory_value_buffer&0b1111111111111);
          //Serial.println(the_data_count);
          if (the_parity_buffer == (the_memory_value_buffer>>d_data_parity_offset)&d_bit_mask){
            the_data_count++;
            Serial.print(((the_memory_value_buffer>>d_data_channel_offset)&0b111));
            Serial.print("\t");
            Serial.println(((the_memory_value_buffer)&0b1111111111));
          }
          else {
            Serial.println(" - parity failed!");
            }
        }
      }
    }
    
  Serial.print(the_timer_configuration_count);
  Serial.println(" timer configurations");
  Serial.print(the_channel_configuration_count);
  Serial.println(" channel configurations");
  Serial.print(the_timestamp_count);
  Serial.println(" timestamp configurations");
  Serial.print(the_data_count);
  Serial.println(" data samples");
  Serial.println("memory full! download data and format eeprom!");

  
  }


unsigned int process_data(bool reset, byte channel, unsigned int data){

  unsigned int the_processed_data_buffer=0;
  bool the_parity_buffer;
  
  the_processed_data_buffer = (data) | (channel<<d_data_channel_offset);
  the_parity_buffer=get_parity(the_processed_data_buffer&0b1111111111111);
  
  the_processed_data_buffer=the_processed_data_buffer | ((unsigned int)the_parity_buffer<<d_data_parity_offset) | ((unsigned int)reset<<d_data_header_offset);

  return the_processed_data_buffer;
  
  }


//void serial_read_flush(){
//  Serial.println(Serial.available());
//  while(Serial.available()!=0) {
//    char t = Serial.read();
//    Serial.println(Serial.available());
//  }
//  delay(1000);
//}  



void loop() {

byte the_command;
unsigned int the_memory_address_buffer;

// check if data has been sent from the computer:
  while (Serial.available()==0) {}
//    // read the most recent byte (which will be from 0 to 255):
    the_command = Serial.read();
    switch (the_command){
      case 'r':
        Serial.println("read command");
          while (Serial.available()==0) {}
          the_command = Serial.read();      
          switch (the_command){
            case 'e':
              Serial.println("eeprom-memory");
              show_eeprom(0,d_eeprom_size);
              break;
            case 'i':
              Serial.println("interpreted data:");
              interpret_eeprom();
              break;
            case 't':
              Serial.println("time-memory");
              break;
            }
        break;
      case 'c':
        Serial.println("configuration command");
          while (Serial.available()==0) {}
          the_command = Serial.read();      
          switch (the_command){
            case 'f':
              Serial.println("frequency configuration");
              while (Serial.available()==0) {}
              the_command = (byte)Serial.read();
              the_timer_configuration = (((unsigned int)the_command)<<d_sample_frequency) | (the_timer_configuration& ~(d_sample_frequency_mask<<d_sample_frequency));
              //Serial.println(the_command);
              //Serial.println(the_timer_configuration);
              break;
            case 'm':
              Serial.println("mode configuration");
              if ((the_timer_configuration>>d_use_minutes)&d_bit_mask) {
                Serial.println("changing mode to seconds");
                the_timer_configuration = (0<<d_use_minutes) | (the_timer_configuration& ~(d_bit_mask<<d_use_minutes));
                }
              else {
                Serial.println("changing mode to minutes");
                the_timer_configuration = (1<<d_use_minutes) | (the_timer_configuration& ~(d_bit_mask<<d_use_minutes));
                }
              break;
            case 'r':
              Serial.println("read configuration");
              read_configuration();
              break;
            case 'w':
              Serial.println("write configuration");
              write_configuration();
              break;
            case 's':
              //Serial.print(the_timer_configuration);
              Serial.println("show configuration");
              //Serial.println(the_timer_configuration);
              Serial.print("frequency: ");
              Serial.print((the_timer_configuration>>d_sample_frequency)&d_sample_frequency_mask);
              if ((the_timer_configuration>>d_use_minutes)&d_bit_mask) {Serial.print(" minutes \n");}
              else {Serial.print(" seconds \n");}
              //Serial.println(the_timer_configuration);

              for (byte c; c<8; c++){
                Serial.print("Channel ");
                Serial.print(c);
                if ((the_channel_configuration>>c)&d_bit_mask){Serial.print(" enabled \n");}
                else {Serial.print(" disabled \n");}
                }
              break;

            case 'e':
              Serial.println("enable channel");
              while (Serial.available()==0) {}
              the_command=((byte)(Serial.read()))-d_ascii_offset;
              //Serial.println(the_command);
                if (the_command<8){
                  the_channel_configuration=((~(d_bit_mask<<the_command))&the_channel_configuration) | 1<<the_command;
                  Serial.print("channel enabled: ");
                  Serial.println(the_command);
                  }
                else {Serial.println("wrong channel number");}
              break;
            
            case 'd':
              Serial.println("disable channel");
              while (Serial.available()==0) {}
              the_command=((byte)(Serial.read()))-d_ascii_offset;
                if (the_command<8){
                  the_channel_configuration=((~(d_bit_mask<<the_command))&the_channel_configuration) | 0<<the_command;
                  Serial.print("channel disabled: ");
                  Serial.println(the_command);
                  }
                else {Serial.println("wrong channel number");}
              break;
            }
        break;

      
      case 'w':
        Serial.println("write command");
        break;
      case 't':
        Serial.println("show time");
        show_time();
        break;
      case 'f':
        Serial.println("format... are you sure?");
        //serial_read_flush();
//        Serial.println(Serial.available());
//        while(Serial.available()!=0) {Serial.read();}
//        Serial.println(Serial.available());
//        while (Serial.available()<1) {Serial.println(Serial.available());}
//        Serial.println(Serial.available());
//        Serial.println(Serial.read());
        while (Serial.available()==0) {}
        the_command = Serial.read();      
        if (the_command=='y'){Serial.println("formatting!");format_eeprom();Serial.println("formatting done!");the_eeprom_data_pointer=get_eeprom_data_pointer();Serial.println("data pointers updated!");}
        else {Serial.println("cancelling!");}
        
        break;

        
    }
//    
//    the_memory_value_buffer=EEPROM.read(the_command);
//    Serial.println(the_memory_value_buffer);
//  }
//}

}
